# Jilk.pw Python Wrapper

## Overview

### About

`jilkpw_py` is a small python module for connecting to [Jilk.pw](https://jilk.pw/)'s Public API. This can help if you would like to embed items from your Discord Server into anything that binds with Python.

### What is Jilk.pw

Jilk.pw is a Discord Server list to help you discover top Discord Servers; with the awesome public Discord Server List!

## Documentation

### Installing

To install `jilkpw_py`, please simply input the following command into your terminal:

```bash
pip3 install jilkpw_py
```

*Please note that on some systems, python3 may be bound to only `pip` & not `pip3`.*

### Using

To use `jilkpw_py`, please open your preffered IDE/text editor and input the following:

#### Get a specific listing

```py
from jilkpw_py import JilkpwWrapper # Import the class

wrapper_name_here = JilkpwWrapper() # Initating an object under `jilkpw_py`

dict_output = wrapper_name_here.find(478363034990149634) # Returns a dict for the given guild_id or throws an exeption

print(dict_output) # Print the output to screen
```

#### Get all listings

```py
from jilkpw_py import JilkpwWrapper # Import the class

wrapper_name_here = JilkpwWrapper() # Initating an object under `jilkpw_py`

dict_output = wrapper_name_here.all() # Returns a dict of all servers on jilkpw or throws an exeption

print(dict_output) # Print the output to screen
```

#### Note

This will give a basic implamentation of `jilkpw_py` but as you can see below, there are various errors you have to catch (hint: try/except around the `dict_output = wrapper_name_here.find(478363034990149634)` line).

### Exceptions

#### HTTP Errors

- `NotFound`: Jilk.pw could not find the guild_id you passed in
- `BadRequest`: A request to Jilk.pw was not handled properly & was aborted
- `NotAuthed`: For whatever reason, you are not authorized to use Jilk.pw's public api
- `WebMoved`: Jilk.pw has moved
- `WebTimeout`: Request to Jilk.pw timed out, please try again later
- `UriTooLong`: The URI is too long for the server to process
- `MiscError`: HTTP response code is `x` (not incl 200 or previously used)

### Misc Errors

- `CannotMain`: If you try to run the module directly

---

**scOwez, 2019**. *Licensed under Apache*
